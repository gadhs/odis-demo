= ODIS Demo
Chris Apsey <christopher.apsey@dhs.ga.gov>
:!toc:

This repo gives a brief overview of a CI-based ODIS replacement.
It uses a handful of policy memos to showcase different production methods.

