= Home and Community Based Services Policy
:imagesdir: ../../images
:index: POL5300
:doctitle: Home and Community Based Services
:polname: home_community_based_services
:docfilesuffix: .adoc

The policy of the Department of Human Services is to provide non-Medicaid-funded, community-based services that support and assist older individuals, persons with disabilities, and their caregivers who may not require intensive medical services to continue living in their homes and communities.
These social and health-related services promote health, self-sufficiency and independence.

== Authority

. https://www.govinfo.gov/app/details/USCODE-1994-title42/USCODE-1994-title42-chap34-subchapI_2-sec3001[Older Americans Act of 1965, as amended, 42 U.S.C. § 3001, et.al]
. https://law.justia.com/codes/georgia/2020/title-49/chapter-6/[O.C.G.A. 49-6-1 et seq] (Services for the Aging)

== References

. https://www.govinfo.gov/app/details/USCODE-1994-title42/USCODE-1994-title42-chap34-subchapI_2-sec3001[Older Americans Act of 1965, as amended, 42 U.S.C. § 3001, et.al]

== Applicability

Area Agencies on Aging (AAA) and/or non-profit organizations fulfill the requirements of the above-stated DHS policy.

== Definitions

* *Older individuals* - persons in the age group 60 and over.
* *Community Based Services* - supportive services such as: Home Delivered Meals, Senior Center Meals and Programs, Adult Day and Adult Day Health, Homemaker Services, Case Management, Health Promotion, Medications Management, Respite Care, Information and Assistance, Employment, Caregiver Support Services and Evidence-Based Health and Wellness programs.

==  Responsibilities

The program administrator of the Livable Communities Section of the Division of Aging Services is responsible for oversight of the development and updating of requirements in the Home and Community Based Services Manual.

include::../../partials/policy_footer.adoc[]

== Evaluation

The Section Manager and respective Program Managers review performance measures monthly to assess program results and evaluate the outcomes of this directive.