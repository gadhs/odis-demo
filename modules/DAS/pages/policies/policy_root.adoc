= Policies
Division of Aging Services, Georgia Department of Human Services
v0.1, 2022-07
:doctype: book
ifdef::backend-pdf[]
:toc: preamble
endif::[]
:toclevels: 3
:imagesdir: ../../images

Some information here about the purpose of this group of policies.
Foreword from the Assistant Deputy Commissioner, outlining appropriate mechanisms for suggesting changes to policies, affirming divisional commitment to transparency and agility, etc. would all be appropriate uses of the space.

ifdef::backend-pdf[]
:leveloffset: +1

include::administration.adoc[]

include::adult_protective_services.adoc[]

include::home_community_based_services.adoc[]

include::long_term_care_ombudsman.adoc[]

include::public_guardianship_adults.adoc[]

:leveloffset: -1
endif::[]